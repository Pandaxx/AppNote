Template.new_note.events({
  "submit .js-new-note"(event, instance) {
    event.preventDefault();
    const titleVal = event.target.title.value;
    const textVal = event.target.text.value;
    Meteor.call('insertNote', titleVal, textVal, Meteor.userId());
    event.target.title.value = "";
    event.target.text.value = "";
  }
});

Template.list_note.helpers({
  notes() {
    return notes.find({ownerId: Meteor.userId()}).fetch();
  }
});

Template.single_note.events({
  "click .js-edit-note"(event, instance) {
    Modal.show("modal_edit_note", instance.data);
  }
});

Template.modal_edit_note.events({
  "submit .js-edit-note"(event, instance) {
    event.preventDefault();
    const idNote = instance.data.note._id;
    const titleVal = event.target.title.value;
    const textVal = event.target.text.value;
    Meteor.call('editNote', idNote, titleVal, textVal);
    Modal.hide();
  },
    "click .js-delete-note"(event, instance) {
      const confimation = confirm("Etes-vous sûr ?");
      if (confimation != true) return;
      Meteor.call('delNote', instance.data.note._id);
      Modal.hide();
    }
});

Template.home.events({
  "click .js-logout"(event, instance) {
    Meteor.logout();
  }
});

