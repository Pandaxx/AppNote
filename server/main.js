import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});


Meteor.methods({
  insertNote(title, text) {
    notes.insert({
      title: title,
      text: text,
      ownerId: Meteor.userId(),
      createdAt: new Date()
    })
  },
  editNote(id, title, text) {
    notes.update({_id: id, }, {
      $set: {
        title: title,
        text: text
      }
    });
  },
  delNote(id) {
      notes.remove({_id: id});
  }
});
