# AppNote
Une application pour prendre des notes

## Features

- Ajouter une note
- Supprimer une note
- Editer une note
- Créer un compte
- Connextion à ce compte


### Photo de l'interface

![Photo de 
l'interface](https://gitlab.com/Pandaxx/AppNote/raw/master/screen/main.PNG)

### Photo de la création d'une note

![Photo de la création de 
note](https://gitlab.com/Pandaxx/AppNote/raw/master/screen/note_create.PNG)

### Photo du panel

![Photo du 
panel](https://gitlab.com/Pandaxx/AppNote/raw/master/screen/panel.PNG)


#### Installation de AppNote

> Installer Météor : https://www.meteor.com/

> git clone https://github.com/SecretsMan/AppNote.git

> cd AppNote

> meteor
